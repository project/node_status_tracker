-- SUMMARY --

This module keeps history of node status whenever we update or insert any node. 
node_status_tracker table contain status information about node.
This module is very useful when we not use workbench or disable node revision.

This module works with new content.

-- REQUIREMENTS --
  No

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --

* Access menu from here : admin/config/search/node-status-tracker

-- CONTACT --

Current maintainers:
* Shravan sonkar https://www.drupal.org/u/shravan-sonkar
  email : shravankumar62757@gmail.com
